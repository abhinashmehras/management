<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\Userinfo as UserResourceinfo;
use Hash;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','device_token','role','account_status','phone_code','phone_number','otp','profile_image','random','rating','time_zone','name_school','address_school','website','school_id','class_incharge'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];



    public function createUser($data){
     
        $createdUser= self::create(
            [
                
                'email'           =>  $data['email']??null,
                'password'        =>  Hash::make( $data['password']),
                'role'            =>  $data['role']??null,
                'name_school'     =>  $data['name_school']??null,
                'address_school'  =>  $data['address_school']??null,
                'phone_number'    =>  $data['phone_number']??null,
                'website'         =>  $data['website']??null,
                'school_id'       =>  $data['school_id']??null,
                'class_incharge'  =>  $data['class_incharge']??null,
            ]
        );
       return $this->user_resource($createdUser);
    }

    
    public function user_resource($user){
        return new UserResource($user);
    }

     public function teacherinfo()
    {
        return $this->hasOne(\App\Models\TeacherInfo::class,"teacher_id","id");
    }


    public function createPassportToken($user){
        return $user->createToken('schoolManagement')->accessToken;
    }
}
