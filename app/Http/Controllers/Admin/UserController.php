<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Models\Role;
use App\User;
use URL;
use Hash;
use Mail;
//use Conekta\Conekta;
use DB;
class UserController extends Controller
{
    protected $serviceObj;
    protected $permotionsObj;
    
    // public function __construct()
    // {
    //      $this->serviceObj     =$service;
    //      $this->permotionsObj  =$permotions;
        
    // }
    

    /**
     * Login page
     */
    public function term(){
     return view('admin.admin.term', ['title' => 'Term and condition']);
    }

    public function privacy(){
     return view('admin.admin.privacy', ['title' => 'Privacy Policy']);
    }

    public function index(){
      if (Auth::check()) {
        return redirect('admin/dashboard');
      }
      Auth::logout();

  
      return view('admin.login', ['title' => 'Login Page']);
    }


    /**
     * Check user Detail
     */

    public function login(Request $request){
         
      $request->validate([
          'email' => 'required|email|exists:users,email',
          'password' => 'required|min:6',
      ]);

      if (!Auth::check()) {
        $email=$request->get('email');
        $password=$request->get('password');
  
        if (Auth::attempt(['email' => $email, 'password' => $password, 'role' =>'admin','account_status'=>'active'])) {
          return redirect('admin/dashboard');
      }else{
        return redirect('admin/login')->with('error', 'Login credential is not valid ') ;
      }
      }
  }
  
  public function logout(){
    Auth::logout();
    return redirect(\URL::previous());
  }

  public function users(Request $request){
        $user=Auth::user();
        $allUser = User::where('role',config('constants.role.USER'))->where('account_status','0')->get();
        return view('admin.admin.viewUser', ['title' => 'Users','user'=>$user, 'allUser'=>$allUser]);
     
  }


}
