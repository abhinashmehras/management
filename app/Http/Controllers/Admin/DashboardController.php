<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Language;
use App\Models\Disease;
use App\Models\Transaction;
use Illuminate\Support\Facades\Auth;
use DB;
class DashboardController extends Controller
{
    public function index(){
        $user        = Auth::user();
        $usersCount  = User::where('role',config('constants.role.USER'))->count();
        $langCount   = Language::get()->count();
        $docCount    = User::where(['role'=>'doctor'])->count();
        $DiseaseCount= Disease::get()->count();
        $Transaction = Transaction::get()->count();
        $earn        = Transaction::get();
        $n=0;
        foreach ($earn as $res) {
        	$n+=$res->amount;
        }
       
        return view('admin.dashboard', ['title' => 'Dashboard','user'=>$user,'usersCount'=>$usersCount,'langCount'=>$langCount,'doctorCount'=>$docCount,'DiseaseCount'=>$DiseaseCount,'transaction'=>$Transaction,'earning'=>$n]);
    }
}
