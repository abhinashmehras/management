<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Redis;
use Hash;
use App\Models\Administrator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\Helper;
use App\Models\Classes;
use App\Models\Presentation;
use App\Models\Subject;
use DB;
use URL;
use Log;
use DateTime;
use Mail;
//use Twilio\Rest\Client;
use App\Repositories\Interfaces\LocationRepositoryInterface;

class SchoolController extends Controller
{
    use \App\Traits\APIResponseManager;
    use \App\Traits\CommonUtil;

    protected $userObj;
    protected $classesObj;
   

    public function __construct(User $user,Classes $classes)
    {
        $this->userObj=$user;
        $this->classesObj=$classes;
    
    }
    
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */

     public function createSubject(Request $request){

         try{
        $request->validate([
            'school_id'      => 'required',
            'title'          => 'required',
            ]);
        
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'BAD_REQUEST', 'error_details', $errorResponse);
        } 

         try{
            $result=\DB::table('subject')->insert(['school_id' => $request->school_id??null,'title'=>$request->title??null,'created_at'=> new \DateTime,'updated_at'=> new \DateTime]);
            return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'CREATESUBJECT');
    } catch (\PDOException $e) {
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }

     }

    public function getSubject(Request $request){
        $data=Subject::get();
        return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'SUCCESS', 'response', $data);
    }

    public function deleteSubject(Request $request){
          try{
        $request->validate([
            'subject_id'      => 'required',
            ]);
        
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'BAD_REQUEST', 'error_details', $errorResponse);
        }

        try{
            $data=Subject::where('id',$request->subject_id)->delete();
            return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'DELETESUBJECT');
    } catch (\PDOException $e) {
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }

    }

}
