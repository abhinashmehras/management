<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Redis;
use Hash;
use App\Models\Administrator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\Helper;
use App\Models\Classes;
use App\Models\Presentation;
use App\Models\Student;
use App\Models\Studentsubject;
use DB;
use URL;
use Log;
use DateTime;
use Mail;
//use Twilio\Rest\Client;
use App\Repositories\Interfaces\LocationRepositoryInterface;

class StudentController extends Controller
{
    use \App\Traits\APIResponseManager;
    use \App\Traits\CommonUtil;

    protected $userObj;
    protected $classesObj;
    protected $studentObj;
   

    public function __construct(User $user,Classes $classes,Student $student)
    {
        $this->userObj=$user;
        $this->classesObj=$classes;
        $this->studentObj=$student;
    
    }
    
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */

     public function createStudent(Request $request){

        try{

        $request->validate([
            'school_id'      => 'required',
            'parent_id'      => 'required',
            'class_id'       => 'required',
            'name'           => 'required',
            'blood_group'    => 'required',
            'age'            => 'required',
            //'profile_image'  => 'required',
            'roll_no'        => 'required',
            'gender'         => 'required',
            'data'           => 'required',
       ]);
        
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }

        try{

        DB::beginTransaction();

        if ($request->hasFile('profile_image')) {
            
            $file = request()->file('profile_image');
            $ext=$file->getClientOriginalExtension();
            $image = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('admin/images/doctor', $image);
            $url= URL::to('/');
            $profile_image=$url.'/admin/images/doctor/'.$image;
            }

            // print_r($request->data);
            // die('==');

            $datas=$this->studentObj->createStudent([
                'school_id'          =>  $request->school_id??null,
                'parent_id'          =>  $request->parent_id??null,
                'class_id'           =>  $request->class_id??null,
                'name'               =>  $request->name??null,
                'blood_group'        =>  $request->blood_group??null,
                'gender'             =>  $request->gender??null,
                'roll_no'            =>  $request->roll_no??null,
                'profile_image'      =>  $profile_image??null,
                'age'                =>  $request->age??null,
                'game'               =>  $request->game??null,
            ]);

            $id=$datas->id;
            foreach($request->data  as $res) {
             
               $result=\DB::table('studentsubject')->insert(['student_id' => $id??null,'subject_id'=>$res['id']??null,'created_at'=> new \DateTime,'updated_at'=> new \DateTime]);
            }
           
        DB::commit();   
        return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'CREATESTUDENT');
    } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }

     }

     public function getStudent(Request $request){
      try{

        $request->validate([
            'parent_id'      => 'required',
            ]);
        
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }

        try{
            $data=Student::where('parent_id',$request->parent_id)->with('studentsubject')->get();
           
        return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'SUCCESS', 'response',  $data);
    } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }

     }

     public function deleteStudent(Request $request){

        try{

        $request->validate([
            'student_id'      => 'required',
            ]);
        
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }
        try{
            $data=Student::where('id',$request->student_id)->delete();

            $result=Studentsubject::where('student_id',$request->student_id)->delete();
           
        return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'DELETESTUDENT');
    } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }
     }

    

}
