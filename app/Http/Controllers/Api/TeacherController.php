<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Redis;
use Hash;
use App\Models\Administrator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\Helper;
use App\Models\Classes;
use App\Models\Presentation;
use App\Models\TeacherInfo;
use App\Models\Teacherclasses;
use App\Models\Teachertimetable;
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Meeting;
use App\Models\Linkcategories;
use App\Models\Linksubcategories;
use DB;
use URL;
use Log;
use DateTime;
use Mail;
//use Twilio\Rest\Client;
use App\Repositories\Interfaces\LocationRepositoryInterface;

class TeacherController extends Controller
{
    use \App\Traits\APIResponseManager;
    use \App\Traits\CommonUtil;

    protected $userObj;
    protected $classesObj;
    protected $TeacherclassesObj;
    protected $TeachertimetableObj;
    protected $LinkcategoriesObj;
   

    public function __construct(User $user,Classes $classes,Teacherclasses $Teacherclasses,Teachertimetable $Teachertimetable,Linkcategories $Linkcategories)
    {
        $this->userObj=$user;
        $this->classesObj=$classes;
        $this->TeacherclassesObj=$Teacherclasses;
        $this->TeachertimetableObj=$Teachertimetable;
        $this->LinkcategoriesObj=$Linkcategories;
    
    }
    
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */

     public function updateteacherProfile(Request $request){

        try{

        $request->validate([
            'teacher_id'        => 'required',
            ]);
        
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }

         try{
            $data=User::where('id',$request->teacher_id)->first();

            if ($request->hasFile('profile_image')) {
            
            $file = request()->file('profile_image');
            $ext=$file->getClientOriginalExtension();
            $profile_image = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('admin/images/doctor', $profile_image);
            $url= URL::to('/');
             $profile_images=$url.'/admin/images/doctor/'.$profile_image;
            
            }

           User::where('id',$request->teacher_id)->update([
                        'name_school'    => $request->name??null,
                        'address_school' => $request->address??null,
                        'profile_image'  => $profile_images??null,
                        'phone_number'   => $request->phone_number??null,
            ]);

           TeacherInfo::where('id',$data->id)->update([
                        'marital_status'   => $request->marital_status??null,
                        'kids'             => $request->kids??null,
                        'blood_group'      => $request->blood_group??null,
                        'allergic'         => $request->allergic??null,
                        'join_date'        => $request->join_date??null,
                        'responsibility'   => $request->responsibility??null,
                        'extra_activity'   => $request->extra_activity??null,
                        'updated_at'       => new \DateTime,
                        'created_at'       => new \DateTime,
            ]);

      return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'PROFILE_UPDATE');

    } catch (\PDOException $e) {
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    } 

     }

     public function getTeacherinfo(Request $request){
         try{
        $request->validate([
            'teacher_id'        => 'required',
            ]);
        
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }

         try{
          
         $data= User::where('id',$request->teacher_id)->with('teacherinfo')->first();
         if(!empty($data->id)){ 
          return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'SUCCESS', 'response',  $data);
         }else{
          return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'USER_NOT_FOUND');  
         }

     } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }
     }

     public function createClasses(Request $request){

         try{
        $request->validate([
            'school_id'         => 'required',
            'teacher_id'        => 'required',
            'title'             => 'required',
            'subject'           => 'required',
            ]);
        
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }
        
        try{
          
        DB::beginTransaction();
            $createdUser=$this->TeacherclassesObj->createClasses([
                'school_id'            =>  $request->school_id??null,
                'teacher_id'           =>  $request->teacher_id??null,
                'title'                =>  $request->title??null,
                'subject'              =>  $request->subject??null,
            ]);
        DB::commit();   
        return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'CLASSCREATED', 'response',  $createdUser);

     } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }
     }

     public function getClassesbyTeacher(Request $request){

         try{

        $request->validate([
            'teacher_id'        => 'required',
            ]);
        
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }

        try{
          
       $data=Teacherclasses::where('teacher_id',$request->teacher_id)->get(); 
        return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'SUCCESS', 'response',  $data);

     } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }
     }

     public function updateClassesbyTeacher(Request $request){

         try{

        $request->validate([
            'id'        => 'required',
            ]);
        
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }

        try{
          
            Teacherclasses::where('id',$request->id)->update([
                             'title'     => $request->title??null,
                             'subject'   => $request->subject??null,
            ]);

        return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'UPDATESUCCESS');

     } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }

     }
     public function createTimeTable(Request $request){

         try{
        $request->validate([
            'class_id'          => 'required',
            'school_id'         => 'required',
            'teacher_id'        => 'required',
            'subject'           => 'required',
            ]);
        
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }
        
        try{
          
        DB::beginTransaction();
            $createdUser=$this->TeachertimetableObj->createTimeTable([
                'class_id'              =>  $request->class_id??null,
                'school_id'             =>  $request->school_id??null,
                'teacher_id'            =>  $request->teacher_id??null,
                'subject'               =>  $request->subject??null,
                'room_no'               =>  $request->room_no??null,
                'adjustment'            =>  $request->adjustment??null,
            ]);
        DB::commit();   
        return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'CLASSTIMETABLE', 'response',  $createdUser);

     } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }
     }

     public function getTimeTableById(Request $request){

        try{
        $request->validate([
            'teacher_id'          => 'required',
            ]);
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }
        try{
            $data=Teachertimetable::where('teacher_id',$request->teacher_id)->get();
       return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'SUCCESS', 'response',  $data);

     } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }

     }

     public function editTimeTable(Request $request){
        
        try{
            $request->validate([
            'timetable_id'          => 'required',
            ]);
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }

         try{


            Teachertimetable::where('id',$request->timetable_id)->update([
                                    'class_id'     => $request->class_id??null,
                                    'school_id'    => $request->school_id??null,
                                    'teacher_id'   => $request->teacher_id??null,
                                    'subject'      => $request->subject??null,
                                    'room_no'      => $request->room_no??null,
                                    'adjustment'   => $request->adjustment??null,
            ]);

       return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'TIMETABLE');

     } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }

     }
    
    public function createSyllabus(Request $request){

      // print_r($request->test);
      // die();

      $result=\DB::table('categories')->insertGetId(['class_id' => $request->class_id??null,'school_id'=>$request->school_id??null,'teacher_id'=>$request->teacher_id??null,'created_at'=> new \DateTime,'updated_at'=> new \DateTime]);
      foreach ($request->data as $res) {

         $record=\DB::table('subcategories')->insertGetId(['categories_id' => $result??null,'subject'=>$res['subject']??null,'syllabus'=>$res['syllabus']??null,'created_at'=> new \DateTime,'updated_at'=> new \DateTime]);
      }
       return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'CREATESYLLABUS');
    } 

    public function deleteSyllabus(Request $request){

        try{
            $request->validate([
            'categories_id'          => 'required',
            ]);
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }

        try{

           $data=Categories::where('id',$request->categories_id)->first();
           $id=$data->id;
           Subcategories::where('categories_id',$id)->delete();
           Categories::where('id',$id)->delete();


       return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'DELETECATEGORIES');

     } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }

    }


    public function updateSyllabus(Request $request){
        try{
            $request->validate([
            'categories_id'          => 'required',
            ]);
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }

        try{
        
        
            Categories::where('id',$request->categories_id)->update([
                        'class_id'       => $request->class_id??null,
                        'school_id'      => $request->school_id??null,
                        'teacher_id'     => $request->teacher_id??null,
                        'created_at'     => new \DateTime,
            ]);

             Subcategories::where('categories_id',$request->categories_id)->delete();


      foreach ($request->data as $res) {

         $record=\DB::table('subcategories')->insertGetId(['categories_id' => $request->categories_id??null,'subject'=>$res['subject']??null,'syllabus'=>$res['syllabus']??null,'created_at'=> new \DateTime,'updated_at'=> new \DateTime]);
    } 
      return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'DELETECATEGORIES');
} catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }
    }

    public function getSyllabus(Request $request){

        try{
            $request->validate([
            'teacher_id'          => 'required',
            ]);
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }

        try{

        $data= Categories::where('teacher_id',$request->teacher_id)->with('subcategory')->get();
      return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'SUCCESS', 'response',  $data);
     } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }
    }

    public function createMeeting(Request $request){
      
      try{
            $request->validate([
            'teacher_id'          => 'required',
            ]);
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }

        try{

       $record=\DB::table('meeting')->insert(['teacher_id' => $request->teacher_id??null,'class_id'=>$request->class_id??null,'school_id'=>$request->school_id??null,'generate_link'=>$request->generate_link??null,'title'=>$request->title??null,'time_schedule'=>$request->time_schedule??null,'created_at'=> new \DateTime,'updated_at'=> new \DateTime]);

      return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'CREATEMEETING');
     } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }

    }

    public function getMeetinglist(Request $request){

        try{
            $request->validate([
            'school_id'          => 'required',
            ]);
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }

      try{
        
      $data=Meeting::where('school_id',$request->school_id)->with('teacher')->with('classes')->get();
      return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'SUCCESS', 'response',  $data);
     } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }
    }

    public function getMeetLinkForStudent(Request $request){

        try{
            $request->validate([
            'class_id'          => 'required',
            ]);
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }

      try{
        
      $data=Meeting::where('class_id',$request->class_id)->with('teacher')->get();
      return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'SUCCESS', 'response',  $data);
     } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }

    }

    public function createHeading(Request $request){
        try{
            $request->validate([
            'teacher_id'      => 'required',
            'school_id'       => 'required',
            'class_id'        => 'required',
            'title'           => 'required',
            ]);
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }

    try{
        
     
      DB::beginTransaction();
            $createdUser=$this->LinkcategoriesObj->createCategories([
                'school_id'            =>  $request->school_id??null,
                'teacher_id'           =>  $request->teacher_id??null,
                'title'                =>  $request->title??null,
                'class_id'             =>  $request->class_id??null,
            ]);
        DB::commit();   
        return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'CREATEHEADING', 'response',  $createdUser);
     } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }
    }

    public function createSuggestion(Request $request){

          try{
            $request->validate([
            'linkcategories_id'      => 'required',
            'name'                   => 'required',
            ]);
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }
        try{
        $result=\DB::table('Linksubcategories')->insert(['linkcategories_id' => $request->linkcategories_id??null,'name'=>$request->name??null,'created_at'=> new \DateTime,'updated_at'=> new \DateTime]);
        
        return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'CREATESUGGESTION');
     } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }

    }

    public function getSuggestion(Request $request){
     
     try{
            $request->validate([
            'school_id'      => 'required',
            'teacher_id'     => 'required',
            'class_id'       => 'required',
            ]);
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }

        try{
        $data=Linkcategories::where('school_id',$request->school_id)->where('teacher_id',$request->teacher_id)->where('class_id',$request->class_id)->with('linksubcategories')->get();
        
         return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'SUCCESS', 'response',  $data);
     } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }

    }

    public function updateSuggestion(Request $request){
        try{
            $request->validate([
            'id'        => 'required',
            'name'      => 'required',
            ]);
         }catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }

    try{
          Linksubcategories::where('id',$request->id)->update([
                                  'name'    => $request->name??null,
            ]);
        
         return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'SUCCESS');
     } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }
    }

    public function deleteSuggestion(Request $request){
        try{
            $request->validate([
            'id'        => 'required',
            ]);
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }

    try{
          Linksubcategories::where('id',$request->id)->delete();
        
         return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'SUGGESTIONDELETE');
     } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }

    }

}
