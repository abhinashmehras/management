<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Redis;
use Hash;
use App\Models\Administrator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\Helper;
use App\Models\Classes;
use App\Models\Presentation;
use App\Models\Timetable;
use DB;
use URL;
use Log;
use DateTime;
use Mail;
//use Twilio\Rest\Client;
use App\Repositories\Interfaces\LocationRepositoryInterface;

class ClassesController extends Controller
{
    use \App\Traits\APIResponseManager;
    use \App\Traits\CommonUtil;

    protected $userObj;
    protected $classesObj;
   

    public function __construct(User $user,Classes $classes)
    {
        $this->userObj=$user;
        $this->classesObj=$classes;
    
    }
    
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */

     public function createClass(Request $request){

        try{

        $request->validate([
            'school_id'        => 'required',
            'class_name'       => 'required',
            'teacher_id'       => 'required',
            ]);
        
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }

        try{

        DB::beginTransaction();

        // if ($request->hasFile('time_table')) {
            
        //     $file = request()->file('time_table');
        //     $ext=$file->getClientOriginalExtension();
        //     $time_table = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
        //     $file->move('admin/images/doctor', $time_table);
        //     $url= URL::to('/');
        //      $time_tables=$url.'/admin/images/doctor/'.$time_table;
            
        //     }

        if ($request->hasFile('date_sheet')) {
            $file = request()->file('date_sheet');
            $ext=$file->getClientOriginalExtension();
            $date_sheet = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('admin/images/doctor', $date_sheet);
            $url= URL::to('/');
            $date_sheets=$url.'/admin/images/doctor/'.$date_sheet;
            }    

      
            $data=$this->classesObj->createClass([
                'school_id'         =>  $request->school_id??null,
                'class_name'        =>  $request->class_name??null,
                'teacher_id'        =>  $request->teacher_id??null,
                //'time_table'        =>  $time_tables??null,
                'date_sheet'        =>  $date_sheets??null,
                'extra_activity'    =>  $request->extra_activity??null,
            ]);

           // $last_id= $data->id;

        
           
        DB::commit();   
        return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'CLASSSUCCESS', 'response',  $data);
    } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }

     }

     public function getClasses(Request $request){

       try{

        $request->validate([
            'school_id'        => 'required',
            ]);
        
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }

        try{

         $data=Classes::where('school_id',$request->school_id)->get();  
        return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'SUCCESS', 'response',  $data);
    } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }

     }

     public function getClassesbyId(Request $request){

        try{
        $request->validate([
            'class_id'        => 'required',
            ]);
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }

         try{

         $data=Classes::where('id',$request->class_id)->first(); 
         $data['today_attendance']='0'; 
         $data['total_student']='0'; 
         return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'SUCCESS', 'response',  $data);

        } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }

     }
     public function getTeacherbyId(Request $request){

         try{
        $request->validate([
            'school_id'        => 'required',
            ]);
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }
        try{

         $data=User::where('school_id',$request->school_id)->get(); 
        
         return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'SUCCESS', 'response',  $data);

        } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }

     }

     public function addTimetable(Request $request){
        try{
        $request->validate([
            'school_id'       => 'required',
            'teacher_id'      => 'required',
            'class_id'        => 'required',
            'day'             => 'required',
            ]);
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }
      try{
         // print_r($request->day);
         // die();
        foreach ($request->data as $res) {
               
              $result=\DB::table('timetable')->insert(['class_id' => $request->class_id??null,'school_id' => $request->school_id??null,'subject'=>$res['subject']??null,'timing'=>$res['timing']??null,'teacher_id'=>$request->teacher_id??null,'day'=>$request->day??null,'room_no'=>$res['room_no']??null,'created_at'=> new \DateTime,'updated_at'=> new \DateTime]);

        }
         
         return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'SUCCESS');

        } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }


     }

     public function searchSubject(Request $request){
        try{
        $request->validate([
            'school_id'       => 'required',
            'class_id'        => 'required',
            'day'             => 'required',
            ]);
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }
        try{
         
       $data= Timetable::where('school_id',$request->school_id)->where('class_id',$request->class_id)->where('day',$request->day)->get();
         
         return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'SUCCESS', 'response',  $data);

        } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }

     }

}
