<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Redis;
use Hash;
use App\Models\Administrator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\Helper;
use App\Models\Presentation;
use App\Models\TeacherInfo;
use App\Models\Events;
use DB;
use URL;
use Log;
use DateTime;
use Mail;
//use Twilio\Rest\Client;
use App\Repositories\Interfaces\LocationRepositoryInterface;

class UserController extends Controller
{
    use \App\Traits\APIResponseManager;
    use \App\Traits\CommonUtil;

    protected $userObj;
   

    public function __construct(User $user)
    {
        $this->userObj=$user;
    
    }
    
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */

    public function signup(Request $request)
    {
        
        try{
        $request->validate([
            'email'          => 'required|email|unique:users',
            'password'       => 'required',
            'role'           => 'required',
            'name'           => 'required',
            'address'        => 'required',
            'phone_number'   => 'required',

            ]);
        
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'BAD_REQUEST', 'error_details', $errorResponse);
        } 
      
        try{
        DB::beginTransaction();
            $createdUser=$this->userObj->createUser([
                'email'            =>  $request->email??null,
                'password'         =>  $request->password??null,
                'role'             =>  $request->role??null,
                'name_school'      =>  $request->name??null,
                'address_school'   =>  $request->address??null,
                'phone_number'     =>  $request->phone_number??null,
                'website'          =>  $request->website??null,
                'school_id'        =>  $request->school_id??null,
                'class_incharge'   =>  $request->class_incharge??null,
            ]);
            $token=$this->userObj->createPassportToken($createdUser);
            $id=$createdUser->id;

            if($createdUser->role =='2'){
                $result=\DB::table('teacherinfo')->insert(['teacher_id' => $createdUser->id]);
            }

            User::where('id',$createdUser->id)->update([
                        'device_token'   => $request->device_token??null,
                        'login_through'  => 'normal',
                       
            ]);

            $createdUser->access_token=$token;
        DB::commit();   
        return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'USER_REGISTER_SUCCESS', 'response',  $createdUser);
    } catch (\PDOException $e) {
        DB::rollback();
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }
     
    }
  
    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */

    public function login(Request $request)
    { 
		 try{
        $request->validate([
            //'email' => 'required|exists:users,email',
            'password'    => 'required|string',
            'email'       => 'required',
            ]);
        
		 } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }

		try{
            
              if(!empty($request->email)){
                $data=User::where('email',$request->email)->first();
                if($data->password ==''){
                  return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'PLEASE_ENTER_PASS', 'response','');
                }
                if (!Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                    return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'USER_PASSWORD_WRONG', 'response','');
                }
                $user=Auth::user();
                $user->device_token=$request->device_token??null;
                $user->save();
                $token=$this->userObj->createPassportToken($user);
                $user->access_token=$token;
                $updatedUser=$this->userObj->user_resource($user);
            }
            User::where('id',$updatedUser->id)->update([
                        'device_token'   => $request->device_token??null,
                        'login_through'  => 'normal',
                       
            ]);
        return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'USER_LOGIN_SUCCESS', 'response', $updatedUser);
    } catch (\PDOException $e) {
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    } 
	}

    public function facebookLogin(Request $request){
      
      try{
        $request->validate([
            'email'       => 'required',
            'type'        => 'required',
            'role'        => 'required',
            ]);
        
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }

        try{
             
              $data= User::where('email',$request->email)->where('role',$request->role)->first();
              if(!empty($data->email)){
                
                $user=$data;
                $user->device_token=$request->device_token??null;
                $user->save();
                $token=$this->userObj->createPassportToken($user);
                $user->access_token=$token;
                $updatedUser=$this->userObj->user_resource($user);
            }else{
           

                $result=\DB::table('users')->insertGetId(['email' => $request->email,'name'=> $request->name??null,'profile_image'=> $request->profile_image??null,'role'=>$request->role??null]);
                if($request->role =='2'){
                $records=\DB::table('teacherinfo')->insert(['teacher_id' => $result]);
                }
                $data=User::where('id',$result)->first();
                $user=$data;
                $user->device_token=$request->device_token??null;
                $user->save();
                $token=$this->userObj->createPassportToken($user);
                $user->access_token=$token;
                $updatedUser=$this->userObj->user_resource($user);
            }
            User::where('id',$updatedUser->id)->update([
                        'device_token'   => $request->device_token??null,
                        'login_through'  => $request->type??null,
                       
            ]);
        return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'USER_LOGIN_SUCCESS', 'response', $updatedUser);
    } catch (\PDOException $e) {
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }

    }

     public function getUserInfo(Request $request){
        
        try{
        $request->validate([
            'id'       => 'required',
            ]);
        
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }

        try{
            
          $data=User::where('id',$request->id)->first();
          if(!empty($data->id)){
             return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'SUCCESS', 'response', $data);
          }else{
           return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'USER_NOT_FOUND');
          }
       
    } catch (\PDOException $e) {
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    } 
    }

    public function updateSchoolProfileInfo(Request $request){
        try{

        $request->validate([
            'id'                => 'required',
            'name_school'       => 'required',
            'address_school'    => 'required',
            'phone_number'      => 'required',
            ]);
        
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }

         try{
            
            User::where('id',$request->id)->update([
                        'name_school'    => $request->name_school??null,
                        'address_school' => $request->address_school??null,
                        'phone_number'   => $request->phone_number??null,
            ]);

            return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'PROFILE_UPDATE');
       
    } catch (\PDOException $e) {
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }

    }

    public function getSchoolList(){
        $data= User::where('role',1)->get();
        return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'SUCCESS', 'response', $data);
    }

    public function getTeacherList(){
        $data= User::where('role',2)->get();
        return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'SUCCESS', 'response', $data);
    }

    public function createEvents(Request $request){
        try{

        $request->validate([
            'school_id'                => 'required',
            'name'                     => 'required',
            ]);
        
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }

         try{

            if ($request->hasFile('image')) {
            
            $file = request()->file('image');
            $ext=$file->getClientOriginalExtension();
            $image = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('admin/images/doctor', $image);
            $url= URL::to('/');
             $images=$url.'/admin/images/doctor/'.$image;
            
            }
            
            $result=\DB::table('events')->insert(['school_id' => $request->school_id??null,'name'=>$request->name??null,'description'=>$request->description??null,'date'=>$request->date??null,'time'=>$request->time??null,'image'=>$images??null,'created_at'=> new \DateTime,'updated_at'=> new \DateTime]);



            return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'EVENTADDED');
       
    } catch (\PDOException $e) {
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }


    }
    public function getEvents(Request $request){

        try{
            $request->validate([
            'school_id'                => 'required',
            ]);
        
         } catch (\Illuminate\Validation\ValidationException $e) {
            $errorResponse = $this->ValidationResponseFormating($e);
            return $this->responseManager(Config('statuscodes.request_status.ERROR'), 
            'BAD_REQUEST', 'error_details', $errorResponse);
        }

        try{

            $data=Events::where('school_id',$request->school_id)->get();
           
             return $this->responseManager(Config('statuscodes.request_status.SUCCESS'), 'SUCCESS', 'response', $data);
       
    } catch (\PDOException $e) {
        $errorResponse = $e->getMessage();
        return $this->responseManager(Config('statuscodes.request_status.ERROR'), 'DB_ERROR', 'error_details', $errorResponse);
    }

    }



  

 

   







    
}
