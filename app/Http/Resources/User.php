<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use URL;
class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       

        
       

        return [
            'id' => $this->id,
            // $this->mergeWhen(config('constants.role.USER') ==$this->role, [
            //     'first_name'  => $this->name,
            //     'last_name'  => $this->last_name,
            //     ]),
            'first_name'  => $this->name??null,
            //'age'         => $age,
            //'last_name'  => $this->last_name,
            'email' =>  $this->email,
            'role'      =>  $this->role,
            'profile_image'=>  is_null($this->profile_image)?'':env('APP_URL')."".env('IMAGE_UPLOAD_PATH').''.$this->profile_image,
            'phone_code'      => $this->phone_code??null,
             $this->mergeWhen(config('constants.role.USER')==$this->role, [
            'gender'          => $gender??null,
            'born'            => $born??null,
            'fathername'      => $fathername??null,
            'chronic_illness' => $chronic_illness??null,
            'marital_status'  => $marital_status??null,
            'education_level' => $education_level??null,
            'chronic_yes'     => $chronic_yes??null,
            'consume_medicines'=>$consume_medicines??null,
            'consume_medicines_yes'=>$consume_medicines_yes??null,
            'allergic_medication'=>$allergic_medication??null,
            'allergic_medication_yes'=>$allergic_medication_yes??null,
            'fractures'=>$fractures??null,
            'supervison'=> $supervison??null,
            'supervison_yes'=>$supervison_yes??null,
            'fractures_yes'=>$fractures_yes??null,
            'address1'=>$address1??null,
            'address2'=>$address2??null,
            'interior_house'=>$interior_house??null,
            'exterior_house'=>$exterior_house??null,
            'zipcode'=>$zipcode??null,
            'country'=>$country??null,
                    ]),
           
            //'user_identification'=>$img,
            'phone_number'       => $this->phone_number??null,
            //'occupation'         => $occupation,
            //'verify_action'      => $this->skip,
            //'is_complete'        => $this->is_complete,
           // 'is_notify'          => $this->is_notify,
           // 'paid_consultation'  => $this->paid_consultation,
            //'free_consultation'  => $this->free_consultation,

            $this->mergeWhen($this->access_token, [
            'access_token'    => $this->access_token,
            ]),

            'otp'             => $this->when($this->otp,$this->otp),
            'location'        => $this->when(
             $this->relationLoaded('location'),
            function () {
                return new Location($this->location);
            }
            ),
            'address'       => $this->address_school,
            'name_school'   => $this->name_school,
            'website'       => $this->website,
            'school_id'     => $this->school_id,
            'class_incharge'=> $this->class_incharge,
            'decice_token'  => $this->decice_token,
            'created_at'    => $this->created_at,
            'updated_at'    => $this->updated_at,
            'business'      => $this->when(
                $this->relationLoaded('business'),
                function () {
                    return new Business($this->business);
                }
            )
        ];
    }
}
