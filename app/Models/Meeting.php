<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    protected $table = 'meeting';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'teacher_id','class_id','school_id','generate_link','title','time_schedule'
    ];

    // public function createStudent($data){
     
    //     return $createdUser= self::create(
    //         [
                
    //             'school_id'         =>  $data['school_id']??null,
    //             'parent_id'         =>  $data['parent_id']??null,
    //             'name'              =>  $data['name']??null,
    //             'class_id'          =>  $data['class_id']??null,
    //             'blood_group'       =>  $data['blood_group']??null,
    //             'age'               =>  $data['age']??null,
    //             'profile_image'     =>  $data['profile_image']??null,
    //             'roll_no'           =>  $data['roll_no']??null,
    //             'game'              =>  $data['game']??null,
    //             'gender'            =>  $data['gender']??null,
    //         ]
    //     );
    //    //return $this->user_resource($createdUser);
    // }

    public function teacher()
    {
        return $this->hasMany(\App\User::class,"id","teacher_id");
    }
     public function classes()
    {
        return $this->hasMany(\App\Models\Classes::class,"id","class_id");
    }
}
