<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Studentsubject extends Model
{
    protected $table = 'studentsubject';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'student_id','subject_id'
    ];

    
}
