<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use App\User;

class Teacherclasses extends Model
{
    protected $table = 'teacherclasses';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'teacher_id','school_id','title','subject',
    ];


     public function createClasses($data){
     
        return $createdUser= self::create(
            [
                
                'teacher_id'     =>  $data['teacher_id']??null,
                'school_id'      =>  $data['school_id']??null,
                'title'          =>  $data['title']??null,
                'subject'        =>  $data['subject']??null,
                
                
            ]
        );
       //return $this->user_resource($createdUser);
    }

  

}
