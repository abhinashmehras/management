<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use App\User;

class TeacherInfo extends Model
{
    protected $table = 'teacherinfo';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'teacher_id','marital_status','kids','blood_group','allergic','join_date','responsibility','extra_activity'
    ];

    public function teacherinfo()
    {
        return $this->hasOne(\App\User::class,"id","teacher_id");
    }

}
