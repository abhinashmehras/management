<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'student';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'school_id','parent_id','name','class_id','blood_group','game','profile_image','age','roll_no','gender'
    ];

    public function createStudent($data){
     
        return $createdUser= self::create(
            [
                
                'school_id'         =>  $data['school_id']??null,
                'parent_id'         =>  $data['parent_id']??null,
                'name'              =>  $data['name']??null,
                'class_id'          =>  $data['class_id']??null,
                'blood_group'       =>  $data['blood_group']??null,
                'age'               =>  $data['age']??null,
                'profile_image'     =>  $data['profile_image']??null,
                'roll_no'           =>  $data['roll_no']??null,
                'game'              =>  $data['game']??null,
                'gender'            =>  $data['gender']??null,
                
                
            ]
        );
       //return $this->user_resource($createdUser);
    }

    public function studentsubject()
    {
        return $this->hasMany(\App\Models\Studentsubject::class,"student_id","id");
    }
}
