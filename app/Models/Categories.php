<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $table = 'categories';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'class_id','school_id','teacher_id'
    ];

  public function subcategory()
    {
        return $this->hasMany(\App\Models\Subcategories::class,"categories_id","id");
    }
}
