<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    protected $table = 'classes';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'school_id','class_name','teacher_id','time_table','date_sheet','extra_activity'
    ];

    public function createClass($data){
     
        return $createdUser= self::create(
            [
                
                'school_id'         =>  $data['school_id']??null,
                'class_name'        =>  $data['class_name']??null,
                'teacher_id'        =>  $data['teacher_id']??null,
                //'time_table'        =>  $data['time_table']??null,
                'date_sheet'        =>  $data['date_sheet']??null,
                'extra_activity'    =>  $data['extra_activity']??null,
                
            ]
        );
       //return $this->user_resource($createdUser);
    }
}
