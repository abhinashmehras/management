<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Linksubcategories extends Model
{
    protected $table = 'linksubcategories';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     
    protected $fillable = [
        'linkcategories','name'
    ];

    // public function createCategories($data){
    //     return $createdUser= self::create(
    //         [
                
    //             'school_id'        =>  $data['school_id']??null,
    //             'class_id'         =>  $data['class_id']??null,
    //             'teacher_id'       =>  $data['teacher_id']??null,
    //             'title'           =>  $data['title']??null,            
    //         ]
    //     );
    // }
}
