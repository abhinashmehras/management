<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teachertimetable extends Model
{
    protected $table = 'teachertimetable';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'class_id','subject','room_no','adjustment','school_id','teacher_id'
    ];

    public function createTimeTable($data){
     
        return $createdUser= self::create(
            [
                
                'class_id'         =>  $data['class_id']??null,
                'teacher_id'       =>  $data['teacher_id']??null,
                'school_id'        =>  $data['school_id']??null,
                'subject'          =>  $data['subject']??null,
                'room_no'          =>  $data['room_no']??null,
                'adjustment'       =>  $data['adjustment']??null,
            ]
        );
       //return $this->user_resource($createdUser);
    }

 
}
