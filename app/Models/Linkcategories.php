<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Linkcategories extends Model
{
    protected $table = 'linkcategories';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'teacher_id','school_id','class_id','title'
    ];

    public function createCategories($data){
        return $createdUser= self::create(
            [
                
                'school_id'        =>  $data['school_id']??null,
                'class_id'         =>  $data['class_id']??null,
                'teacher_id'       =>  $data['teacher_id']??null,
                'title'           =>  $data['title']??null,            
            ]
        );
    }

     public function linksubcategories()
    {
        return $this->hasMany(\App\Models\Linksubcategories::class,"linkcategories_id","id");
    }
}
