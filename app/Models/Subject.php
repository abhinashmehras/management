<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table = 'subject';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'school_id','title'
    ];

    // public function createClass($data){
     
    //     return $createdUser= self::create(
    //         [
                
    //             'school_id'         =>  $data['school_id']??null,
    //             'class_name'        =>  $data['class_name']??null,
    //             'teacher_id'        =>  $data['teacher_id']??null,
    //             'time_table'        =>  $data['time_table']??null,
    //             'date_sheet'        =>  $data['date_sheet']??null,
    //             'extra_activity'    =>  $data['extra_activity']??null,
                
    //         ]
    //     );
    // }
}
