@include('admin.includes.sidebar-skin')
<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            <li class="nav-item nav-profile">
              <div class="nav-link">
                <div class="profile-image">
                  @php
                    $user=Auth::user();
                  
                    @endphp

                  <img src="{{asset($user->profile_image)}}" alt="image" />
                  <span class="online-status online"></span> <!--change class online to offline or busy as needed-->
                </div>
                <div class="profile-name">
                  <p class="name">
                  @yield('user_name')
                  </p>
                  <p class="designation">
                  @yield('role')
                  </p>
                </div>
              </div>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="{{url('admin/dashboard')}}">
                <i class="icon-rocket menu-icon"></i>
                <span class="menu-title">Dashboard </span>
                 <!--<span class="badge badge-success">New</span> -->
              </a>
            </li>

            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#page-layouts" aria-expanded="false" aria-controls="page-layouts">
                <i class="icon-user menu-icon"></i>
                <span class="menu-title">Patient</span>
                <!-- <span class="badge badge-danger">3</span> -->
              </a>
              <div class="collapse" id="page-layouts">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item d-none d-lg-block"> <a class="nav-link" href="{{url('admin/users')}}">View Patient</a>
                  </li>
                  <li class="nav-item d-none d-lg-block"> <a class="nav-link" href="{{url('admin/patientInformation')}}">Patient Information</a>
                  </li>
                </ul>
              </div>
            </li>

            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#page-layoutss" aria-expanded="false" aria-controls="page-layoutss">
                <i class="icon-user menu-icon"></i>
                <span class="menu-title">Doctor</span>
                <!-- <span class="badge badge-danger">3</span> -->
              </a>
              <div class="collapse" id="page-layoutss">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item d-none d-lg-block"> <a class="nav-link" href="{{url('admin/doctors')}}">Save Doctors</a>
                  </li>
                  <li class="nav-item d-none d-lg-block"> <a class="nav-link" href="{{url('admin/approveddoctors')}}">Approved Doctors</a>
                  </li>

                </ul>
              </div>
            </li>

           <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#survey-layouts" aria-expanded="false" aria-controls="page-layouts">
                <i class="mdi mdi-settings menu-icon"></i>
                <span class="menu-title">Settings</span>
                <!-- <span class="badge badge-danger">3</span> -->
              </a>
              <div class="collapse" id="survey-layouts">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item d-none d-lg-block"> <a class="nav-link" href="{{url('admin/permotionsList')}}">Permotions</a>
                  <li class="nav-item d-none d-lg-block"> <a class="nav-link" href="{{url('admin/language')}}">Language</a>
                  </li>

                  <li class="nav-item d-none d-lg-block"> <a class="nav-link" href="{{url('admin/video')}}">Video</a>
                  </li>
                  <li class="nav-item d-none d-lg-block"> <a class="nav-link" href="{{url('admin/disease')}}">Disease</a>
                  </li>
                  <li class="nav-item d-none d-lg-block"> <a class="nav-link" href="{{url('admin/services')}}">Services</a>
                  </li>
                  <li class="nav-item d-none d-lg-block"> <a class="nav-link" href="{{url('admin/administrator')}}">Root of administrator</a>
                  </li>
                  <li class="nav-item d-none d-lg-block"> <a class="nav-link" href="{{url('admin/paymentPercentage')}}">Payment Percentage</a>
                  </li>
                   <li class="nav-item d-none d-lg-block"> <a class="nav-link" href="{{url('admin/presentation')}}">Presentation</a>
                  </li>

                </ul>
              </div>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="{{url('admin/viewPayment')}}">
                <i class="icon-briefcase menu-icon"></i>
                <span class="menu-title">Payment Transaction </span>
                 <!--<span class="badge badge-success">New</span> -->
              </a>
            </li>

              
          </ul>
        </nav>
        <!-- partial -->