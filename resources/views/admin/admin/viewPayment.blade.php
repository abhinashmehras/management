@extends('admin.layouts.app')
@section('title',$title)
@section('user_name',$user->name)
@section('role',$user->role)
@section('content')
        
        <div class="content-wrapper" style="min-height: 1545px;">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Payment History</h4>
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                     <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th width="2%">Series</th>
                            <th width="2%">Image</th>
                            <th width="7%">Patient Name</th>
                            <th width="7%">Patient Email</th>
                            <th width="6%">Doctor ID</th>
                            <th width="7%">Payment Staus</th>
                            <th width="2%">Amount</th>
                            <th width="2%">Admin Status</th>
                            <th width="7%">Created Date</th>
                            <th width="2%">Actions</th>
                        </tr>
                      </thead>
                       <tbody>
                       @php $i=0; @endphp
                        @foreach($data as $user)
                        @php $i++; @endphp
                        <tr>
                          <td> @php echo $i; @endphp</td>
                             
                             @php if(!empty($user->patient['profile_image'])){ @endphp
                          <td><img src="{{asset($user->patient['profile_image'])}}" alt="image" /></td>
                          @php }else{ @endphp

                             <td><img src="{{asset('admin/images/dummy-image.jpg')}}" alt="image" /></td>
                           @php } @endphp
                        
                            <td>{{@$user->patient['name']}}</td>
                            <td>{{@$user->patient['email']}}</td>
                            <td>{{@$user->doctor['random']}}</td>
                            <td style="color:green;">{{@$user->status}}</td>
                            <td>${{@$user->amount}}</td>
                              @if($user->admin_status == '0')
                                    <td><label class="badge {{$user->admin_status == '0' ? 'badge-success' : 'badge-danger'}}"> {{$user->admin_status == '0' ? 'Pending' : 'Paid'}}</label></td>
                                    @else
                                    <td><label class="badge {{$user->admin_status == '0' ? 'badge-success' : 'badge-danger'}}"> {{$user->admin_status == '0' ? 'Pending' : 'Paid'}}</label></td>
                                    @endif
                            <td>{{@$user->created_at}}</td>
                            <td>
                              <ul class="navbar-nav">
                                <li class="nav-item dropdown d-none d-lg-flex">
                                  <a class="nav-link  nav-btn" id="actionDropdown" href="#" data-toggle="dropdown">
                                    <button class="btn btn-outline-primary">Action</button>
                                  </a>
                                  <div class="dropdown-menu navbar-dropdown" aria-labelledby="actionDropdown">
                                     <a href="{{route('viewTransaction',$user->id)}}" class="dropdown-item" >View</a>
                                  </div>
                                </li>
                              </ul>
                            </td>
                        </tr>
                         @endforeach
                      </tbody>

                      <tbody>
                       
                      </tbody>
                    </table>                   
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<script type="text/javascript">
  function block_confirmation(id, status)
  {
    swal({
        title: "Are you sure you want to "+status+"?",
        text: "Please ensure and then confirm",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#ab8be4",
        confirmButtonText: "Yes, "+status+" it!",
        closeOnConfirm: false
    })
   
    .then((willDelete) => {
      if (willDelete) {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
          type: 'GET',
           url: "{{route('block_user')}}?user_id="+id+"&status="+status,
          success:function(data){
            if(data.success == true)
            {
              swal("Done!", data.message, "success");
            }
            else
            {
              swal("Error!", data.message, "error");
            }
            setTimeout(function(){ location.reload()}, 3000);
          }
        });
      } 
    });
  }

   function delete_confirmation(id)
  {
    swal({
        title: "Are you sure want to delete this user?",
        text: "Please ensure and then confirm",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#ab8be4",
        confirmButtonText: "Yes",
        closeOnConfirm: false
    })
   
    .then((willDelete) => {
      if (willDelete) {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
          type: 'GET',
          url: "{{route('delete_user')}}?user_id="+id,
          success:function(data){
            if(data.success == true)
            {
              swal("Done!", data.message, "success");
            }
            else
            {
              swal("Error!", data.message, "error");
            }
            setTimeout(function(){ location.reload()}, 3000);
          }
        });
      } 
    });
  }
 
</script>
@endsection