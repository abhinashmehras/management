<p>AVISO DE PRIVACIDAD </p>

<p>HERBEN TECH S.A.P.I DE C.V. , mejor conocido como VINKU , con domicilio en calle Mariano Arista #1085, colonia Tequisquiapan, ciudad San Luis Potos&iacute;, municipio o delegaci&oacute;n San Luis Potos&iacute;, c.p. 78250, en la entidad de San Luis Potos&iacute;, pa&iacute;s M&eacute;xico y portal de internet www.vinku.mx , es el responsable del uso y protecci&oacute;n de sus datos personales, y al respecto le informamos lo siguiente:   &iquest;Para qu&eacute; fines utilizaremos sus datos personales?</p>

<p> De manera adicional, utilizaremos su informaci&oacute;n personal para las siguientes finalidades secundarias que no son necesarias para el servicio solicitado, pero que nos permiten y facilitan brindarle una mejor atenci&oacute;n: &#9679; PUBLICAR PUBLICIDAD &#9679; IDENTIFICAR MERCADO &#9679; PEDIDOS &#9679; PUBLICIDAD DE TERCEROS En caso de que no desee que sus datos personales se utilicen para estos fines secundarios, ind&iacute;quelo a continuaci&oacute;n:</p>

<p>No consiento que mis datos personales se utilicen para los siguientes fines:</p>

<p>[ ] PUBLICAR PUBLICIDAD [ ] IDENTIFICAR MERCADO [ ] PEDIDOS [ ] PUBLICIDAD DE TERCEROS</p>

<p>La negativa para el uso de sus datos personales para estas finalidades no podr&aacute; ser un motivo para que le neguemos los servicios y productos que solicita o contrata con nosotros.   &iquest;Qu&eacute; datos personales utilizaremos para estos fines?   Para llevar a cabo las finalidades descritas en el presente aviso de privacidad, utilizaremos los siguientes datos personales: &#9679; Nombre &#9679; Estado Civil &#9679; Registro Federal de Contribuyentes(RFC) &#9679; Clave &uacute;nica de Registro de Poblaci&oacute;n (CURP) &#9679; Identificaci&oacute;n Oficial (INE O IFE) &#9679; Lugar de nacimiento &#9679; Fecha de nacimiento &#9679; Nacionalidad &#9679; Domicilio /Ciudad/ Pa&iacute;s &#9679; Tel&eacute;fono particular &#9679; Tel&eacute;fono celular &#9679; Correo electr&oacute;nico &#9679; Firma aut&oacute;grafa &#9679; Firma electr&oacute;nica &#9679; Edad &#9679; Fotograf&iacute;a &#9679; Color de la piel &#9679; Color del cabello &#9679; Se&ntilde;as particulares &#9679; Estatura &#9679; Peso &#9679; Cicatrices &#9679; Tipo de sangre  &#9679; Genero &#9679; Edad &#9679; Enfermedades cr&oacute;nicas &#9679; Medicamentos que consume de manera peri&oacute;dica &#9679; Alergias &#9679; Fracturas u operaciones previas &#9679; N&uacute;mero de hijos &#9679; Medicamentos recomendados &#9679; Nivel de educaci&oacute;n &#9679; Estatus Civil</p>

<p>Adem&aacute;s de los datos personales mencionados anteriormente, para las finalidades informadas en el presente aviso de privacidad utilizaremos los siguientes datos personales considerados como sensibles, que requieren de especial protecci&oacute;n: &#9679; Datos sobre ideolog&iacute;a; creencias religiosas, filos&oacute;ficas o morales; opiniones pol&iacute;ticas y/o afiliaci&oacute;n sindical &#9679; Datos de salud &#9679; Datos sobre vida sexual &#9679; Datos de origen &eacute;tnico o racial</p>

<p> &iquest;C&oacute;mo puede acceder, rectificar o cancelar sus datos personales, u oponerse a su uso?   Usted tiene derecho a conocer qu&eacute; datos personales tenemos de usted, para qu&eacute; los utilizamos y las condiciones del uso que les damos (Acceso). Asimismo, es su derecho solicitar la correcci&oacute;n de su informaci&oacute;n personal en caso de que est&eacute; desactualizada, sea inexacta o incompleta (Rectificaci&oacute;n); que la eliminemos de nuestros registros o bases de datos cuando considere que la misma no est&aacute; siendo utilizada adecuadamente (Cancelaci&oacute;n); as&iacute; como oponerse al uso de sus datos personales para fines espec&iacute;ficos (Oposici&oacute;n). Estos derechos se conocen como derechos ARCO.   Para el ejercicio de cualquiera de los derechos ARCO, usted deber&aacute; presentar la solicitud respectiva a trav&eacute;s del siguiente medio:</p>

<p>Enviando un correo electr&oacute;nico a: vinku.app@gmail.com</p>

<p>Para conocer el procedimiento y requisitos para el ejercicio de los derechos ARCO, ponemos a su disposici&oacute;n el siguiente medio:</p>

<p>ENVIAR CORREO ELECTRONICO A vinku.app@gmail.com</p>

<p>Los datos de contacto de la persona o departamento de datos personales, que est&aacute; a cargo de dar tr&aacute;mite a las solicitudes de derechos ARCO, son los siguientes:   a) Nombre de la persona o departamento de datos personales: DATOS PERSONALES b) Domicilio: calle Mariano Arista #1085, colonia Tequisquiapan, ciudad SAN LUIS POTOSI, municipio o delegaci&oacute;n SAN LUIS POTOSI, c.p. 78250, en la entidad de SAN LUIS POTOSI, pa&iacute;s M&eacute;xico c) Correo electr&oacute;nico: vinku.app@gmail.com d) N&uacute;mero telef&oacute;nico: 444 340 7202</p>

<p>Usted puede revocar su consentimiento para el uso de sus datos personales</p>

<p>Usted puede revocar el consentimiento que, en su caso, nos haya otorgado para el tratamiento de sus datos personales. Sin embargo, es importante que tenga en cuenta que no en todos los casos podremos atender su solicitud o concluir el uso de forma inmediata, ya que es posible que por alguna obligaci&oacute;n legal requiramos seguir tratando sus datos personales. Asimismo, usted deber&aacute; considerar que para ciertos fines, la revocaci&oacute;n de su consentimiento implicar&aacute; que no le podamos seguir prestando el servicio que nos solicit&oacute;, o la conclusi&oacute;n de su relaci&oacute;n con nosotros.</p>

<p>Para revocar su consentimiento deber&aacute; presentar su solicitud a trav&eacute;s del siguiente medio:</p>

<p>Enviando un correo electr&oacute;nico a vinku.app@gmail.com</p>

<p>Con relaci&oacute;n al procedimiento y requisitos para la revocaci&oacute;n de su consentimiento, le informamos lo siguiente:</p>

<p>a) &iquest;A trav&eacute;s de qu&eacute; medios pueden acreditar su identidad el titular y, en su caso, su representante, as&iacute; como la personalidad este &uacute;ltimo? NOMBRE COMPLETO</p>

<p>b) &iquest;Qu&eacute; informaci&oacute;n y/o documentaci&oacute;n deber&aacute; contener la solicitud? CORREO ELECTRONICO</p>

<p>c) &iquest;En cu&aacute;ntos d&iacute;as le daremos respuesta a su solicitud? 5 DIAS HABILES</p>

<p>d) &iquest;Por qu&eacute; medio le comunicaremos la respuesta a su solicitud? CORREO ELECTRONICO</p>

<p>e) Para mayor informaci&oacute;n sobre el procedimiento, ponemos a disposici&oacute;n los siguientes medios: vinku.app@gmail.com   &iquest;C&oacute;mo puede limitar el uso o divulgaci&oacute;n de su informaci&oacute;n personal?   Con objeto de que usted pueda limitar el uso y divulgaci&oacute;n de su informaci&oacute;n personal, le ofrecemos los siguientes medios:</p>

<p>correo a vinku.app@gmail.com</p>

<p> Asimismo, usted se podr&aacute; inscribir a los siguientes registros, en caso de que no desee obtener publicidad de nuestra parte:</p>

<p>Registro P&uacute;blico de Usuarios, para mayor informaci&oacute;n consulte el portal de internet de la CONDUSEF</p>

<p>El uso de tecnolog&iacute;as de rastreo en nuestro portal de internet</p>

<p>Le informamos que en nuestra p&aacute;gina de internet utilizamos cookies, web beacons u otras tecnolog&iacute;as, a trav&eacute;s de las cuales es posible monitorear su comportamiento como usuario de internet, as&iacute; como brindarle un mejor servicio y experiencia al navegar en nuestra p&aacute;gina. Los datos personales que recabamos a trav&eacute;s de estas tecnolog&iacute;as, los utilizaremos para los siguientes fines:</p>

<p>Enviarles ofertas, promociones y para contactarlos para cualquier fin informativo o comercial</p>

<p>Los datos personales que obtenemos de estas tecnolog&iacute;as de rastreo son los siguientes:</p>

<p>Identificadores, nombre de usuario y contrase&ntilde;as de una sesi&oacute;n Idioma preferido por el usuario Regi&oacute;n en la que se encuentra el usuario Tipo de navegador del usuario Tipo de sistema operativo del usuario Fecha y hora del inicio y final de una sesi&oacute;n de un usuario P&aacute;ginas web visitadas por un usuario</p>

<p> Para conocer la forma en que se pueden deshabilitar estas tecnolog&iacute;as, consulte el siguiente medio:</p>

<p>Enviando un correo electr&oacute;nico a vinku.app@gmail.com</p>

<p> &iquest;C&oacute;mo puede conocer los cambios en este aviso de privacidad?   El presente aviso de privacidad puede sufrir modificaciones, cambios o actualizaciones derivadas de nuevos requerimientos legales; de nuestras propias necesidades por los productos o servicios que ofrecemos; de nuestras pr&aacute;cticas de privacidad; de cambios en nuestro modelo de negocio, o por otras causas.</p>

<p>Nos comprometemos a mantenerlo informado sobre los cambios que pueda sufrir el presente aviso de privacidad, a trav&eacute;s de: Enviando un correo electr&oacute;nico.</p>

<p>El procedimiento a trav&eacute;s del cual se llevar&aacute;n a cabo las notificaciones sobre cambios o actualizaciones al presente aviso de privacidad es el siguiente:</p>

<p>Enviando un correo electr&oacute;nico</p>