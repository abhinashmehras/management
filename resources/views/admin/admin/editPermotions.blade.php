@extends('admin.layouts.app')
@section('title',$title)
@section('user_name',$user->name)
@section('role',$user->role)
@section('content')
        
      <div class="content-wrapper">
          <div class="row">
             <h4 class="card-title">Edit Permotions</h4>
            <div class="col-md-12 d-flex align-items-stretch grid-margin">
              <div class="row flex-grow">
                <div class="col-6 grid-margin">
                  <div class="card">
                    <div class="card-body">
                      <form action="{{url('admin/UpdatePermotions')}}" method="POST" class="forms-sample" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                          <label for="exampleInputEmail1">Heading</label>
                          <input type="text" name="heading" value="{{$data->heading}}" class="form-control" id="exampleInputEmail1" placeholder="Enter email" required="">
                          <input type="hidden" name="id" value="{{$data->id}}" class="form-control" id="exampleInputEmail1" placeholder="Enter email" required="">
                             </div>

                             <div class="form-group">
                          <label for="exampleInputEmail1">Heading (Spanish)</label>
                          <input type="text" name="heading_es" value="{{$data->heading_es}}" class="form-control" id="exampleInputEmail1" placeholder="Enter email" required="">
                        
                             </div>


                          <div class="form-group">
                          <label for="exampleInputEmail1">Off</label>
                          <input type="text" name="off" value="{{$data->off}}" class="form-control" id="exampleInputEmail1" placeholder="Enter email" required="">
                          </div>
                          <div class="form-group">
                          <label for="exampleInputPassword1">Upload Image</label>
                           <input type="file" name="image" id="image" class="file-upload-default" >
                      <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload Image" required="">
                        <span class="input-group-btn">
                          <button class="file-upload-browse btn btn-info" type="button">Upload</button>
                        </span>
                       
                      </div>
                      <span id="official_ident" style="color: red;"></span>
                       <img src="{{$data->image}}" style="width: 45px; height: 45px;">
                        </div>
                         <button type="submit" class="btn btn-success mr-2">Submit</button>
                         
                     </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
                
          </div>
        </div>

@endsection