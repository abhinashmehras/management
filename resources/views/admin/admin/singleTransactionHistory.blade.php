@extends('admin.layouts.app')
@section('title',$title)
@section('user_name',$user->name)
@section('role',$user->role)
@section('content')
<div class="content-wrapper">
   <div class="row">
    <div class="profile-data" style="border-radius: 8px;margin:auto;width:80% !important;background-color: #FFFFFF; padding: 25px 20px;">
   <div class="col-md-12  grid-margin stretch-card">
          
    
  
      <div class="row">
          <div class="col-md-12">
              <h4 style="opacity: 0.9;color: #000000;font-family: Graphik;font-size: 16px;font-weight: 500;letter-spacing: 0;line-height: 24px;margin: 0;padding-bottom: 20px;">Payment Details</h4>
              @if(!is_null(@$user->profile_image))
              <div style="position:relative">
              <img src="{{env('APP_URL')."".env('IMAGE_UPLOAD_PATH').'/'.$user->profile_image}}" alt="" class="profileImg2" style="width: 114px;border-radius: 50%;height: 114px !important;">
              </div>
              @else

              <div class="first_letter">
                  <span>{{strtoupper(substr(@$user->name,0,1))}}</span>
              </div>

             @endif
              <div class="row">
                  <div class="col-md-4">
                      <h3 style="color: rgba(0, 0, 0, 0.44);font-family: Graphik;font-size: 14px;font-weight: 500;letter-spacing: 0;line-height: 24px;margin: 0;display: flex;">Name</h3>
                      <p>{{@$data->patient['name']}}</p>
                  </div>
                  <div class="col-md-4">
                      <h3 style="color: rgba(0, 0, 0, 0.44);font-family: Graphik;font-size: 14px;font-weight: 500;letter-spacing: 0;line-height: 24px;margin: 0;display: flex;">Email Id</h3>
                      <p>{{@$data->patient['email']}} <img src="{{asset('web/images/tick.png')}}" alt=""
                              style="all: unset;">
                      </p>
                  </div>
                  <div class="col-md-4">
                      <h3 style="color: rgba(0, 0, 0, 0.44);font-family: Graphik;font-size: 14px;font-weight: 500;letter-spacing: 0;line-height: 24px;margin: 0;display: flex;">Transaction Id</h3>
                         <p>{{@$data->transaction_id}}</p>
                      </p>
                  </div>
                  <div class="col-md-4">
                     <h3 style="color: rgba(0, 0, 0, 0.44);font-family: Graphik;font-size: 14px;font-weight: 500;letter-spacing: 0;line-height: 24px;margin: 0;display: flex;">Status</h3>
                      {{@$data->status}}
                  </div>
                  <div class="col-md-4">
                    <h3 style="color: rgba(0, 0, 0, 0.44);font-family: Graphik;font-size: 14px;font-weight: 500;letter-spacing: 0;line-height: 24px;margin: 0;display: flex;">Amount</h3>
                    <p>${{@$data->amount}}
                    </p>
                </div>
                <div class="col-md-4">
                    <h3 style="color: rgba(0, 0, 0, 0.44);font-family: Graphik;font-size: 14px;font-weight: 500;letter-spacing: 0;line-height: 24px;margin: 0;display: flex;">Currency</h3>
                    <p>{{@$data->currency}}
                    </p>
                </div>
                <div class="col-md-4">
                    <h3 style="color: rgba(0, 0, 0, 0.44);font-family: Graphik;font-size: 14px;font-weight: 500;letter-spacing: 0;line-height: 24px;margin: 0;display: flex;">Code</h3>
                    <p>{{@$data->code}}
                    </p>
                </div>
              </div>
               @if(@$user->admin_status == '0')
              <a href="{{route('changeStatus',$data->id)}}"  class="btn btn-success mr-2">Pending</a>
               @else
               <a href="#" disabled class="btn btn-success mr-2">Paid</a>
               @endif
             

          

              
          </div>
      </div>
  </div>

            </div>
   </div>
</div>
<style>
 

</style>
@endsection
