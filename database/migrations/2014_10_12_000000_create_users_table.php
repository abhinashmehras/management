<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('role');
            $table->string('profile_image')->nullable();
            $table->string('name_school')->nullable();
            $table->string('address_school')->nullable();
            $table->string('account_status')->default(0);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('phone_code')->nullable();
            $table->string('class_incharge')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('login_through')->nullable();
            $table->string('otp')->nullable();
            $table->string('website')->nullable();
            $table->string('school_id')->nullable();
            $table->string('type')->nullable();
            $table->string('device_token')->nullable();
            $table->string('password')->nullable();
            $table->string('block_status')->default(0);
            $table->string('delete_status')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
