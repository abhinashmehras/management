<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::create('classes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('school_id')->nullable();
            $table->string('class_name')->nullable();
            $table->string('teacher_id')->nullable();
            $table->string('time_table')->nullable();
            $table->string('date_sheet')->nullable();
            $table->text('extra_activity')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
