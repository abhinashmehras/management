<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::create('student', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('school_id')->nullable();
            $table->string('patent_id')->nullable();
            $table->string('name')->nullable();
            $table->string('gender')->nullable();
            $table->string('roll_no')->nullable();
            $table->string('game')->nullable();
            $table->string('age')->nullable();
            $table->string('profile_image')->nullable();
            $table->string('class_id')->nullable();
            $table->string('blood_group')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
