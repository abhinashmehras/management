<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('get-user-details', function (Request $request) {
  
      return ['status' => 200, 'user' => $request->user() ];
})->middleware('auth:api');

Route::namespace('Api')->group(function () {
     
     Route::post('login', 'UserController@login');
     Route::post('signup', 'UserController@signup');
     Route::post('facebookLogin', 'UserController@facebookLogin');
     Route::get('getSchoolList', 'UserController@getSchoolList');
     Route::post('getClasses', 'ClassesController@getClasses');
     Route::middleware(['auth:api'])->group(function () {
        Route::post('getUserInfo', 'UserController@getUserInfo');
        Route::post('updateSchoolProfileInfo', 'UserController@updateSchoolProfileInfo');
        Route::get('getTeacherList', 'UserController@getTeacherList');
        Route::post('createClass', 'ClassesController@createClass');
        Route::post('getClassesbyId', 'ClassesController@getClassesbyId');
        Route::post('updateteacherProfile', 'TeacherController@updateteacherProfile');
        Route::post('getTeacherinfo', 'TeacherController@getTeacherinfo');
        Route::post('getTeacherbyId', 'ClassesController@getTeacherbyId');
        Route::post('createEvents', 'UserController@createEvents');
        Route::post('getEvents', 'UserController@getEvents');
        Route::post('createClasses', 'TeacherController@createClasses');
        Route::post('getClassesbyTeacher', 'TeacherController@getClassesbyTeacher');
        Route::post('updateClassesbyTeacher', 'TeacherController@updateClassesbyTeacher');
        Route::post('createTimeTable', 'TeacherController@createTimeTable');
        Route::post('getTimeTableById', 'TeacherController@getTimeTableById');
        Route::post('editTimeTable', 'TeacherController@editTimeTable');
        Route::post('createSubject', 'SchoolController@createSubject');
        Route::get('getSubject', 'SchoolController@getSubject');
        Route::post('deleteSubject', 'SchoolController@deleteSubject');
        Route::post('createSyllabus', 'TeacherController@createSyllabus');
        Route::post('deleteSyllabus', 'TeacherController@deleteSyllabus');
        Route::post('updateSyllabus', 'TeacherController@updateSyllabus');
        Route::post('getSyllabus', 'TeacherController@getSyllabus');
        Route::post('createStudent', 'StudentController@createStudent');
        Route::post('getStudent', 'StudentController@getStudent');
        Route::post('deleteStudent', 'StudentController@deleteStudent');
        Route::post('createMeeting', 'TeacherController@createMeeting');
        Route::post('getMeetinglist', 'TeacherController@getMeetinglist');
        Route::post('getMeetLinkForStudent', 'TeacherController@getMeetLinkForStudent');
        Route::post('createHeading', 'TeacherController@createHeading');
        Route::post('createSuggestion', 'TeacherController@createSuggestion');
        Route::post('getSuggestion', 'TeacherController@getSuggestion');
        Route::post('updateSuggestion', 'TeacherController@updateSuggestion');
        Route::post('deleteSuggestion', 'TeacherController@deleteSuggestion');
        Route::post('addTimetable', 'ClassesController@addTimetable');
        Route::post('searchSubject', 'ClassesController@searchSubject');
     
    
    
     
     });
});