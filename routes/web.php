<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


    
Route::namespace('Admin')->prefix('admin')->group(function () {
    Route::get('login','UserController@index')->name('login');
    Route::post('check_user','UserController@login');
    Route::get('term','UserController@term');
    Route::get('privacy','UserController@privacy');
    
    Route::middleware(['auth'])->group(function () {
        Route::get('dashboard','DashboardController@index');
        Route::get('viewPayment','PaymentController@viewPayment');
        Route::get('logout','UserController@logout');
        Route::get('users','UserController@users');
        Route::get('patientInformation','UserController@patientInformation');
        
        //Route::get('viewUser/{id}','UserController@viewUser');
        Route::get('/viewUser/{id}',['as'=>'viewUser', 'uses' => 'UserController@viewUser']);
        Route::get('/trustedDoctor/{id}',['as'=>'trustedDoctor', 'uses' => 'UserController@trustedDoctor']);
        
        Route::get('/paidChatDoctor/{id}',['as'=>'paidChatDoctor', 'uses' => 'UserController@paidChatDoctor']);
        

        Route::get('/viewTransaction/{id}',['as'=>'viewTransaction', 'uses' => 'PaymentController@viewTransaction']);
        
        Route::get('/changeStatus/{id}',['as'=>'changeStatus', 'uses' => 'PaymentController@changeStatus']);
         
        
        Route::get('/user/block',['as'=>'block_user','uses' => 'UserController@block_user']);
        Route::get('/user/approved',['as'=>'block_user','uses' => 'UserController@approved']);
        Route::get('/user/delete',['as'=>'delete_user','uses' => 'UserController@delete_user']);

        //Doctors
        Route::get('doctors','DoctorsController@index');
        //approveddoctors
        Route::get('presentation','UserController@presentation');
        
        Route::get('viewDoctors','DoctorsController@viewDoctors');
        Route::get('approveddoctors','DoctorsController@approveddoctors');
        Route::get('adddoctors','DoctorsController@add');
        Route::post('UpdateDoctor','DoctorsController@UpdateDoctor');
       
       
        

        Route::get('language','LanguageController@index');
        Route::get('video','LanguageController@video');
        Route::get('viewLanguage','LanguageController@viewLanguage');
        Route::get('addlanguage','LanguageController@addlanguage');

        Route::get('addDisease','DoctorsController@addDisease');
        
        Route::post('admin/add','LanguageController@add');
        Route::post('createDisease','DoctorsController@createDisease');
        Route::post('createServices','UserController@createServices');
        Route::post('createPermotions','UserController@createPermotions');


        Route::get('payment','UserController@payment');
        
        

        Route::post('addDoctor','DoctorsController@addDoctor');
        
        Route::post('createAdministaration','UserController@createAdministaration');
        
        Route::get('addadministration','UserController@addadministration');
        
        Route::get('disease','DoctorsController@disease');
        Route::get('services','UserController@services');
        Route::get('administrator','UserController@administrator');
        
        Route::get('permotionsList','UserController@permotionsList');
        
        Route::post('uploadVideo','LanguageController@uploadVideo');
        
        Route::get('/viewDoctor/{id}',['as'=>'viewDoctor', 'uses' => 'DoctorsController@viewDoctor']);
        Route::get('/editVideo/{id}',['as'=>'editVideo', 'uses' => 'LanguageController@editVideo']);
        Route::get('/editdisease/{id}',['as'=>'editdisease', 'uses' => 'DoctorsController@editdisease']);
        
         Route::get('/EditDoctor/{id}',['as'=>'EditDoctor', 'uses' => 'DoctorsController@EditDoctor']);
        

        Route::post('addlanguage','LanguageController@languages_add');
        Route::get('addServices','UserController@addServices');
        Route::get('addPermotions','UserController@addPermotions');
        
        Route::post('updateServices','UserController@updateServices');

        Route::post('UpdatePermotions','UserController@UpdatePermotions');
        Route::post('updatePercentage','UserController@updatePercentage');
        Route::get('addPresentation','UserController@addPresentation');
        Route::post('createPresentation','UserController@createPresentation');
        
        
        Route::get('paymentPercentage','UserController@paymentPercentage');
      
        Route::post('updateAdministaration','UserController@updateAdministaration');
        
        
        Route::post('updatelanguage','LanguageController@updatelanguage');
        Route::post('updateDeaseas','DoctorsController@updateDeaseas');
        Route::get('/user/deleteService',['as'=>'delete_services','uses' => 'UserController@delete_services']);

        Route::get('/user/delete_presentation',['as'=>'delete_presentation','uses' => 'UserController@delete_presentation']);
        
       Route::get('/user/delete_root',['as'=>'delete_root','uses' => 'UserController@delete_root']);

        Route::get('/deletePermotions',['as'=>'delete_permotions','uses' => 'UserController@delete_permotions']);
        
        Route::get('/user/deleteLanguage',['as'=>'delete_lang','uses' => 'LanguageController@delete_lang']);
        Route::get('/deleteDisease',['as'=>'delete_disease','uses' => 'DoctorsController@delete_disease']);
        
        Route::get('/user/editlang/{id}',['as'=>'editLang', 'uses' => 'LanguageController@editLang']);
        Route::get('/editServices/{id}',['as'=>'editServices', 'uses' => 'UserController@editServices']);
        
         Route::get('/editroot/{id}',['as'=>'editroot', 'uses' => 'UserController@editroot']);
        
        Route::get('/editPermotions/{id}',['as'=>'editPermotions', 'uses' => 'UserController@editPermotions']);

        Route::get('/emailShoot/{id}',['as'=>'emailShoot', 'uses' => 'UserController@emailShoot']);

        
        

        
    });
});

// Route::namespace('Web')->group(function () {
//     Route::get('/','HomeController@index')->name('home');
//     Route::resource('login','LoginController');
// });